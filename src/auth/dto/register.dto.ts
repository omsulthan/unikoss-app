import { IsEmail, IsNotEmpty, IsOptional } from 'class-validator';

export enum statusUser {
  AKTIF = 'aktif',
  TERTUNDA = 'tertunda',
  BLOKIR = 'blokir',
}

export class RegisterDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  phone_numbers: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;

  @IsOptional()
  status: string;

  @IsOptional()
  image: string;

  @IsOptional()
  role_name: string;
}
