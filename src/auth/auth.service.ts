import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateFacilityDto } from 'src/kos/facility/dto/update-kos.dto';
import { Role } from 'src/users/entities/role.entity';
import { User } from 'src/users/entities/user.entity';
import { EntityNotFoundError, Repository } from 'typeorm';
import { RegisterDto, statusUser } from './dto/register.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { error } from 'console';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Role)
    private roleRepository: Repository<User>,
  ) {}

  async create(registerDto: RegisterDto) {
    const role = await this.roleRepository.findOne({
      where: {
        name: registerDto.role_name,
      },
    });

    const password = registerDto.password;
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(password, 10);

    if (role.name === 'penyewa') {
      const result = await this.userRepository.insert({
        name: registerDto.name,
        password: hash,
        role: role,
        phone_numbers: registerDto.phone_numbers,
        status: statusUser.AKTIF,
        email: registerDto.email,
        salt: salt,
      });
      return {
        statusCode: HttpStatus.OK,
        message: 'Register Berhasil',
      };
    } else {
      const result = await this.userRepository.insert({
        name: registerDto.name,
        password: hash,
        role: role,
        phone_numbers: registerDto.phone_numbers,
        status: statusUser.TERTUNDA,
        email: registerDto.email,
        salt: salt,
      });
      return {
        statusCode: HttpStatus.OK,
        message: 'Register Berhasil',
      };
    }
  }

  async findByEmail(email: string) {
    const user = await this.userRepository.findOneBy({ email });
    if (user) {
      return user
    } else {
      return false;
    }
  }

  async login(login: LoginDto) {
    try {
      const existUser = await this.userRepository.findOne({
        where: {
          email: login.email,
        },
        relations: ['role'],
      });
      const password = login.password;
      if (existUser && (await bcrypt.compare(password, existUser.password))) {
        const accessToken = this.jwtService.sign({
          existUser: {
            email: existUser.email,
            name: existUser.name,
            status: existUser.status,
            role: existUser.role.name,
            image: existUser.image
          }
        });
        return {
          statusCode: HttpStatus.CREATED,
          token: accessToken,
        };
      } else {
        return {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: 'username/password salah!',
        };
      }
    } catch (e) {
      return e;
    }
  }

  findAll() {
    return this.userRepository.findAndCount();
  }

  async findOne(id: string) {
    try {
      return await this.userRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async update(id: string, updateFacilityDto: UpdateFacilityDto) {
    try {
      await this.userRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.userRepository.update(id, updateFacilityDto);

    return this.userRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async remove(id: string) {
    try {
      await this.userRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.userRepository.delete(id);
  }
}
