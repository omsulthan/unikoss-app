import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @Post('/register')
  async register(@Body() registerDto: RegisterDto) {
    const cekEmail = await this.authService.findByEmail(registerDto.email);
    if (cekEmail) {
      return {
        statusCode: HttpStatus.CONFLICT,
        message: 'email already exist!',
      };
    }

    return this.authService.create(registerDto);
  }

  @Post('/login')
  login(@Body() body: LoginDto) {
    return this.authService.login(body);
  }

  @Get('/login/validate')
  @UseGuards(AuthGuard())
  validate(@Req() req) {
    console.log('ini reqnya', req.user);
    // return this.authService.validate(payload)
  }
}
