import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {
    // kita perlu set lagi params dari parent class kita
    super({
      // pastikan secret key
      secretOrKey: 'topSecret19',
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    });
  }

  async validate(payload: any) {
    const existUser = await this.userRepository.findOneBy({
      email: payload.existUser.email,
    });

    if (!existUser) {
      throw new HttpException(
        {
          statusCode: HttpStatus.UNAUTHORIZED,
          message: 'UNAUTHORIZED!',
          data: 'Token is invalid!',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }
    return existUser;
  }
}
