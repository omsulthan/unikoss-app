import { Module } from '@nestjs/common';
import { Transaction } from './entities/transaction.entity';
import { TransactionController } from './transaction.controller';
import { TransactionService } from './transaction.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BoardingHouse } from 'src/kos/entities/boarding_house.entity';
import { AuthModule } from 'src/auth/auth.module';
import { User } from 'src/users/entities/user.entity';
import { UsersModule } from '../users/users.module';
import { KosModule } from 'src/kos/kos.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Transaction, BoardingHouse]),
    AuthModule,
    UsersModule,
    KosModule,
  ],
  controllers: [TransactionController],
  providers: [TransactionService],
  exports: [TransactionService]
})
export class TransactionModule {}
