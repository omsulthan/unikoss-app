import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class CreateTransactionDto {
  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  start_date: string;

  @IsNotEmpty()
  end_date: string;

  @IsOptional()
  invoice: string;
}
