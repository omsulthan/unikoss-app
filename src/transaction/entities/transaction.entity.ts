import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { BoardingHouse } from '../../kos/entities/boarding_house.entity';

export enum statusTransaction {
  TERTUNDA = 'tertunda',
  BERHASIL = 'berhasil',
  TERTOLAK = 'tertolak',
  BATAL = 'batal',
}

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(
    () => {
      return User;
    },
    (callback) => {
      return callback.transaction;
    },
  )
  user: User;

  @ManyToOne(
    () => {
      return BoardingHouse;
    },
    (callback) => {
      return callback.id;
    },
  )
  boarding: BoardingHouse;

  @Column()
  price: number;

  @Column()
  start_date: string;

  @Column()
  end_date: string;

  @Column({
    type: 'enum',
    enum: statusTransaction,
    default: statusTransaction.TERTUNDA,
  })
  status: statusTransaction;

  @Column({ nullable: true })
  invoice: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
