import {
  Body,
  Put,
  Controller,
  Get,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Post,
  Delete,
  Query,
  UseInterceptors,
  UploadedFile,
  Req,
  UseGuards,
  Res,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { TransactionService } from './transaction.service';
import { AuthGuard } from '@nestjs/passport';
import multer, { diskStorage } from 'multer';
import { storage } from 'src/helper/save-storage';
import { updateTransactionDto } from './dto/update-transaction.dto';

@Controller('transaction')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  // Get All List Transaction

  @Get()
  async AllTransaction(
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
  ) {
    const data = await this.transactionService.getAllTransaction(
      page,
      pageSize,
    );

    return {
      data,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // Create Transaction
  @Post(':id')
  @UseGuards(AuthGuard())
  async createTransaction(
    @Body() createTransactionDto: CreateTransactionDto,
    @Req() req,
    @Param('id') id: string,
  ) {
    return {
      data: await this.transactionService.createTransaction(
        createTransactionDto,
        req.user.id,
        id,
      ),
      statusCode: HttpStatus.CREATED,
      message: 'success',
    };
  }

  // Get Transaction By Id
  @Get(':id')
  async findById(@Param('id') id: string) {
    return {
      data: await this.transactionService.findById(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('/recent/tr')
  async recentTransaction(
    @Query('page') page: number,
    @Query('pageSize') pageSize,
  ) {
    return this.transactionService.recentTransaction(page, pageSize);
  }
  @Get('/all/my-order')
  @UseGuards(AuthGuard())
  findMyTransaction(@Req() req) {
    return this.transactionService.findMyTransaction(req.user.id);
  }

  @Get('all/my-order/:id')
  @UseGuards(AuthGuard())
  findMyOrder(@Param('id') id: string) {
    return this.transactionService.findMyOrder(id);
  }

  // Get Payment
  @Get('/payment/testing')
  async findPayment() {
    return {
      data: await this.transactionService.getPayment(),
      statusCode: HttpStatus.OK,
      message: 'Berhasil',
    };
  }

  @Get('approval/my-kos/:id')
  @UseGuards(AuthGuard())
  async findMyApprovalTransaction(@Param('id') id: string, @Req() req) {
    const result = await this.transactionService.findMyApprovalTransaction(
      id,
      req.user,
    );
    return {
      data: result,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
  @Get('approval/my-kos')
  @UseGuards(AuthGuard())
  async findAllMyTransaction(@Req() req) {
    const result = await this.transactionService.findAllMyTransaction(req);
    return {
      data: result,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // Create Pembayaran
  @Put(':id')
  async updateTransaction(
    @Body() updateTransactionDto: updateTransactionDto,
    @Param('id', ParseUUIDPipe) id: string,
  ) {
    const data = this.transactionService.updateTransaction(
      id,
      updateTransactionDto.invoice,
    );
    return {
      data: 'Pembayaran Berhasil',
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete(':id')
  async deleteTransaction(@Param('id', ParseUUIDPipe) id: string) {
    return await this.transactionService.cancelTransaction(id);
  }

  // Approval Status
  @Put(':status/:id')
  async updateStatusTransaction(
    @Param('id', ParseUUIDPipe) id: string,
    @Param('status') status: string,
  ) {
    return {
      data: await this.transactionService.updateStatusTransaction(status, id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // Cancel Transaction
  @Delete(':id')
  async cancelTransaction(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.transactionService.cancelTransaction(id),
      statusCode: HttpStatus.OK,
      message: 'Transaksi anda berhasil dibatalkan',
    };
  }

  // Upload File
  @Post('file')
  uploadFileAndPassValidation(
    @UploadedFile()
    file: Express.Multer.File,
  ) {
    return {
      file: file.buffer.toString,
    };
  }

  @Get('/image/:imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './files' });
  }
}
