import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import e from 'express';
import { User } from 'src/users/entities/user.entity';
import { Repository, EntityNotFoundError, In } from 'typeorm';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { statusTransaction, Transaction } from './entities/transaction.entity';
import { UsersService } from '../users/users.service';
import { KosService } from 'src/kos/kos.service';

// map((v) => {
//   v.nama
//   v.img.map(img => {
//     <img src={`http://localhost:3222/${img}`} />
//   })
// })

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Transaction)
    private trasactionRepository: Repository<Transaction>,
    private usersService: UsersService,
    private kosService: KosService,
  ) {}

  // Create Transaction
  async createTransaction(
    createTransactionDto: CreateTransactionDto,
    userId: string,
    boardingId: string,
  ) {
    const userData = await this.usersService.findOne(userId);
    const boardingData = await this.kosService.findOne(boardingId);
    await this.kosService.updateStatus(boardingData.id);

    const result = await this.trasactionRepository.save({
      ...createTransactionDto,
      user: userData,
      boarding: boardingData,
    });

    return result;
  }

  async findById(id: string) {
    try {
      const query = this.trasactionRepository
        .createQueryBuilder('transaction')
        .where('transaction.id = :id', { id: id })
        .innerJoinAndSelect('transaction.user', 'user')
        .innerJoinAndSelect('transaction.boarding', 'boarding')
        .innerJoinAndSelect('boarding.user', 'owner')
        .getOne();
      return query;
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw e;
      } else {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: `Transaksi dengan id: ${id} tidak ditemukan `,
          },
          HttpStatus.NOT_FOUND,
        );
      }
    }
  }

  async findMyTransaction(id: string) {
    const data = await this.trasactionRepository.findAndCount({
      where: {
        user: {
          id,
        },
        status: In([statusTransaction.TERTUNDA, statusTransaction.BERHASIL]),
      },
      relations: [
        'boarding',
        'boarding.image',
        'boarding.facilities',
        'boarding.user',
        'boarding.district',
        'boarding.village',
      ],
    });

    return data[0];
  }

  async findMyApprovalTransaction(id: string, req) {
    const data = await this.trasactionRepository.findOneOrFail({
      where: {
        id,
      },
      relations: [
        'boarding',
        'boarding.image',
        'boarding.facilities',
        'boarding.user',
      ],
    });
    return data;
  }

  async findAllMyTransaction(req) {
    const boarding = await this.kosService.findMyKos(req.user.id);
    const boardingId = boarding.data.map((v) => {
      return v.id;
    });

    const transaction = await this.trasactionRepository.findAndCount({
      where: {
        boarding: {
          id: In(boardingId),
        },
      },
      relations: ['boarding'],
    });
    return transaction;
  }

  async findMyOrder(id: string) {
    const data = await this.trasactionRepository.findOneOrFail({
      where: {
        id,
      },
      relations: [
        'boarding',
        'boarding.image',
        'boarding.user',
        'boarding.review',
        'boarding.review.user',
        'boarding.facilities',
        'boarding.district',
        'boarding.city',
      ],
    });

    return data;
  }
  // Create Pembayaran
  async updateTransaction(id: string, image: string) {
    try {
      await this.trasactionRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: `Transaksi dengan id: ${id} tidak ditemukan `,
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
    console.log(image);
    await this.trasactionRepository.save({ id, invoice: image });

    return this.trasactionRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  // Aprroval Status
  async updateStatusTransaction(status: string, id: string) {
    try {
      const transaction = await this.trasactionRepository.findOneOrFail({
        where: {
          id,
        },
        relations: ['boarding'],
      });
      const boardingData = await this.kosService.findOne(
        transaction.boarding.id,
      );
      await this.kosService.filledStatus(boardingData.id);
      if (status == 'approve') {
        var result = statusTransaction.BERHASIL;
      } else if (status == 'reject') {
        var result = statusTransaction.TERTOLAK;
      } else if (status != 'approve' && 'reject') {
        throw new HttpException(
          {
            statusCode: HttpStatus.METHOD_NOT_ALLOWED,
            message: `Method ${status} Not Found`,
          },
          HttpStatus.NOT_FOUND,
        );
      }

      await this.trasactionRepository.save({
        id,
        status: result,
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
    return this.trasactionRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async getAllTransaction(page: number, pageSize: number) {
    const query = this.trasactionRepository
      .createQueryBuilder('transaction')
      .innerJoinAndSelect('transaction.user', 'user')
      .innerJoinAndSelect('transaction.boarding', 'boarding')
      .skip(page * (pageSize || 10))
      .take(pageSize || 10);
    const result = await query.getManyAndCount();
    return {
      data: result[0],
      count: result[1],
    };
  }

  async cancelTransaction(id: string) {
    try {
      const transaction = await this.trasactionRepository.findOneOrFail({
        where: {
          id,
        },
        relations: ['boarding'],
      });
      await this.trasactionRepository.save({
        id,
        status: statusTransaction.BATAL,
      });
      await this.kosService.cancelStatus(transaction.boarding.id);
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            message: `Transaksi dengan id ${id} tidak ditemukan`,
          },
          HttpStatus.NOT_FOUND,
        );
      }
    }
  }

  async remove(id: string, boardingId: string) {
    await this.trasactionRepository.save({
      id,
      status: statusTransaction.BATAL,
    });

    await this.kosService.cancelStatus(boardingId);
  }

  getPayment() {
    const result = this.trasactionRepository
      .createQueryBuilder('transaction')
      .where('transaction.invoice IS NOT NULL')
      .getMany();

    return result;
  }

  async recentTransaction(page: number, pageSize: number) {
    const result = this.trasactionRepository.find({
      take: pageSize,
      skip: page,
      relations: ['user', 'boarding', 'boarding.user'],
    });

    return result;
  }
}
