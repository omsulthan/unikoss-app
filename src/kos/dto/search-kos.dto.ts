import { IsOptional } from "class-validator";

export class SearchKosDto {
    @IsOptional()
    province: string

    @IsOptional()
    city: string

    @IsOptional()
    district: string

    @IsOptional()
    village: string
}