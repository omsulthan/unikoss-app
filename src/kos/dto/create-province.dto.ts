import { IsNotEmpty } from 'class-validator';

export class CreateProvinceDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  name: string;

}
