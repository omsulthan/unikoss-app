import { PartialType } from "@nestjs/mapped-types";
import { IsArray, IsNotEmpty, IsOptional } from "class-validator";
import { CreateDateColumn } from "typeorm";
import { Facility } from "../entities/facility.entity";
import { CreateKosDto } from "./create-kos.dto";

export class UpdateKosDto {

    @IsOptional()
    name: string;
  
    @IsOptional()
    type: string

    @IsOptional()
    price: number

    @IsOptional()
    status: string
  
    @IsOptional()
    description: string
  
    @IsOptional()
    notes: string
  
    @IsOptional()
    facilities

}