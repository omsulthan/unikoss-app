import { IsArray, isNotEmpty, IsNotEmpty, IsOptional } from 'class-validator';
import { City } from '../entities/city.entity';
import { District } from '../entities/district.entity';
import { Province } from '../entities/province.entity';
import { Village } from '../entities/village.entity';

export class CreateKosDto {
  
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  type: string

  @IsArray()
  images: [];

  @IsOptional()
  province_id: Province;

  @IsOptional()
  city_id: City;

  @IsOptional()
  district_id: District;

  @IsOptional()
  village_id: Village;

  @IsOptional()
  user_id: string;

  @IsNotEmpty()
  address: string

  @IsNotEmpty()
  price: number

  @IsNotEmpty()
  description: string

  @IsNotEmpty()
  notes: string

  @IsArray()
  listFacility

  @IsArray()
  listNewFacility

  // @IsArray()
  // image


}
