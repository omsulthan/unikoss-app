import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { CreateKosDto } from './dto/create-kos.dto';
import { UpdateKosDto } from './dto/update-kos.dto';
import { KosService } from './kos.service';
import { ParseUUIDPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('kos')
export class KosController {
  constructor(private readonly kosService: KosService) {}

  @Post()
  @UseGuards(AuthGuard())
  createKosan(@Body() createKosDto: CreateKosDto, @Req() req) {
    return this.kosService.createKosan(createKosDto, req.user.id);
  }

  @Put('/editkos/:id')
  @UseGuards(AuthGuard('jwt'))
  updateKos(@Param('id') id: string, @Body() updateKosDto: UpdateKosDto) {
    return this.kosService.update(id, updateKosDto);
  }

  @Put('/button/:action/:id')
  approve(@Param('id') id: string, @Param('action') action: string) {
    return this.kosService.approve(id, action);
  }

  @Put('/delete/:id')
  deleteKos(@Param('id') id: string) {
    return this.kosService.remove(id);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  findAll(@Req() req) {
    return this.kosService.findMyKos(req.user.id);
  }

  @Get('/allkos')
  allKos() {
    return this.kosService.findAll();
  }

  @Get('/detail/:id')
  @UseGuards(AuthGuard('jwt'))
  async findById(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.kosService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('/city/:city')
  async findByCity(@Param('city') city: string) {
    return this.kosService.findKosByCity(city);
  }
  @Get('/recomend')
  async findRecomendKos() {
    return {
      data: await this.kosService.RecomendationKos(),
    };
  }

  @Get('/popular')
  async findPopularKos() {
    return await this.kosService.popularKos();
  }

  @Get('/status/:status')
  async findByStatus(
    @Param('status') status: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
  ) {
    return this.kosService.findByStatus(status, page, pageSize);
  }

  @Get('/search/:search')
  searchKosan(
    @Param('search') search: string,
    @Query('type') type: string,
    @Query('page') page: number,
    @Query('pageSize') pageSize: number,
    @Query('province') province: string,
    @Query('city') city: string,
    @Query('district') district: string,
    @Query('village') village: string,
  ) {
    return this.kosService.searchKosan(
      search,
      type,
      page,
      pageSize,
      province,
      city,
      district,
      village,
    );
  }
}
