import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { EntityNotFoundError, In, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BoardingHouse } from './entities/boarding_house.entity';
import { CreateKosDto } from './dto/create-kos.dto';
import { UpdateKosDto } from './dto/update-kos.dto';
import { Province } from './entities/province.entity';
import { Facility } from './entities/facility.entity';
import { networkInterfaces } from 'os';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { UsersService } from 'src/users/users.service';
import { Image } from './entities/image.entity';
import { ReviewService } from './review/review.service';
import { Review } from './entities/review.entity';
import { promiseTimeout } from '@nestjs/terminus/dist/utils';

@Injectable()
export class KosService {
  constructor(
    @InjectRepository(BoardingHouse)
    private boardingRepository: Repository<BoardingHouse>,
    @InjectRepository(Facility)
    private facilityRepository: Repository<Facility>,
    @InjectRepository(Image)
    private imageRepository: Repository<Image>,
    private userService: UsersService,
    @InjectRepository(Image)
    private reviewRepository: Repository<Review>,
  ) {}

  async create(createKosDto: CreateKosDto) {
    const result = await this.boardingRepository.insert(createKosDto);

    return this.boardingRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }

  approve(id: string, action: string) {
    if (action === 'approve') {
      return this.boardingRepository.save({
        id,
        status: 'tersedia',
      });
    } else if (action === 'reject') {
      return this.boardingRepository.save({
        id,
        status: 'tertolak',
      });
    }
  }

  async popularKos() {
    const kos = await this.boardingRepository.find({
      relations: ['review'],
    });
  }

  async createKosan(createKosDto: CreateKosDto, req: string) {
    const userData = await this.userService.findOne(req);
    const existFacility = await this.facilityRepository.find({
      where: {
        name: In(createKosDto.listFacility),
      },
    });

    await createKosDto.listNewFacility.map((facilityName) => {
      const facility = new Facility();
      facility.name = facilityName;
      facility.typeof_facility = 'fasilitas tambahan';
      existFacility.push(facility);
    });

    const kosan = new BoardingHouse();
    (kosan.name = createKosDto.name),
      (kosan.type = createKosDto.type),
      (kosan.address = createKosDto.address),
      (kosan.status = 'tertunda'),
      (kosan.price = createKosDto.price),
      (kosan.description = createKosDto.description),
      (kosan.notes = createKosDto.notes),
      (kosan.user = userData),
      (kosan.province = createKosDto.province_id),
      (kosan.city = createKosDto.city_id),
      (kosan.district = createKosDto.district_id),
      (kosan.village = createKosDto.village_id),
      (kosan.facilities = existFacility);
    await this.boardingRepository.save(kosan);

    createKosDto.images.map((v: any) => {
      const image = new Image();
      image.name = v.url;
      image.boarding = kosan;
      this.imageRepository.insert(image);
    });

    return {
      statusCode: HttpStatus.OK,
      message: 'berhasil membuat kosan',
    };
  }

  async findAll() {
    const kos = await this.boardingRepository.findAndCount({
      where: {
        status: 'tersedia',
      },
      relations: ['review', 'image', 'user', 'city'],
    });
    return {
      data: kos[0],
      count: kos[1],
    };
  }

  async findKosByCity(city: string) {
    const data = await this.boardingRepository.find({
      where: {
        status: 'tersedia',
        city: {
          name: city,
        },
      },
      relations: ['image', 'village', 'district'],
    });

    return data;
  }

  async findMyKos(id: string) {
    const data = await this.boardingRepository.findAndCount({
      where: {
        user: {
          id,
        },
      },
      relations: ['image', 'village', 'district'],
    });

    const dataBody = data[0]?.map((d) => {
      return {
        ...d,
        image: d.image[0],
        count: data[1],
      };
    });

    return {
      data: dataBody,
    };
  }

  async searchKosan(
    search: string,
    type: string,
    page: number,
    pageSize: number,
    province: string,
    city: string,
    district: string,
    village: string,
  ) {
    const kosan = this.boardingRepository.find({
      where: {
        status: 'tersedia',
        province: { id: province },
        city: { id: city },
        district: { id: district },
        village: { id: village },
      },
      take: pageSize,
      skip: page,
      relations: ['image', 'district', 'village', 'city'],
    });

    return kosan;
  }

  async findByStatus(status: string, page: number, pageSize: number) {
    // const result = await this.boardingRepository.find({
    //   where: {
    //     status,
    //   },

    //   relations: ['image', 'user'],
    // });
    const baseQuery = this.boardingRepository
      .createQueryBuilder('b')
      .where('b.status = :status', { status: status })
      .leftJoinAndSelect('b.image', 'i')
      .innerJoinAndSelect('b.user', 'u');

    return {
      data: await baseQuery
        .skip(page || 1 * (pageSize || 10))
        .take(pageSize || 10)
        .getMany(),
    };
  }

  async findOne(id: string) {
    try {
      const boarding = await this.boardingRepository.findOne({
        where: {
          id,
        },
        relations: [
          'review',
          'review.user',
          'facilities',
          'image',
          'user',
          'district',
          'city',
        ],
      });

      return {
        ...boarding,

        rating: boarding.rate,
      };
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async RecomendationKos() {
    const kos = await this.boardingRepository.find({
      order: {
        rate: 'desc',
      },
      relations: ['image'],
    });
    return kos;
  }

  // async popular(){
  //   const kos = await this.boardingRepository.find({

  //   })
  // }

  async update(id: string, updateKosDto: UpdateKosDto) {
    // try {
    //   await this.boardingRepository.findOneOrFail({
    //     where: {
    //       id,
    //     },
    //   });
    // } catch (e) {
    //   if (e instanceof EntityNotFoundError) {
    //     throw new HttpException(
    //       {
    //         statusCode: HttpStatus.NOT_FOUND,
    //         error: 'Data not found',
    //       },
    //       HttpStatus.NOT_FOUND,
    //     );
    //   } else {
    //     throw e;
    //   }
    // }

    const exist = await this.facilityRepository.find({
      where: {
        name: In(updateKosDto.facilities),
      },
    });

    await Promise.all(
      updateKosDto.facilities.map(async (v) => {
        const exists = await this.facilityRepository.findOneBy({ name: v });
        if (!exists) {
          const facility = new Facility();
          facility.name = v;
          facility.typeof_facility = 'fasilitas tambahan';
          exist.push(facility);
        }
      }),
    );

    const boarding = this.boardingRepository.create();
    boarding.id = id;
    (boarding.name = updateKosDto.name),
      (boarding.type = updateKosDto.type),
      (boarding.description = updateKosDto.description),
      (boarding.status = updateKosDto.status),
      (boarding.notes = updateKosDto.notes),
      (boarding.price = updateKosDto.price);
    (boarding.facilities = exist), await this.boardingRepository.save(boarding);

    return this.boardingRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  async updateStatus(id: string) {
    try {
      await this.boardingRepository.findOneOrFail({
        where: {
          id,
        },
      });
      await this.boardingRepository.save({ id, status: 'terpesan' });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: `Kos dengan id: ${id} tidak ditemukan `,
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }
  async filledStatus(id: string) {
    try {
      await this.boardingRepository.findOneOrFail({
        where: {
          id,
        },
      });
      await this.boardingRepository.save({ id, status: 'terisi' });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: `Kos dengan id: ${id} tidak ditemukan `,
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async cancelStatus(id: string) {
    try {
      await this.boardingRepository.findOneOrFail({
        where: {
          id,
        },
      });
      await this.boardingRepository.save({ id, status: 'tersedia' });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: `Kos dengan id: ${id} tidak ditemukan `,
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async remove(id: string) {
    try {
      await this.boardingRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.boardingRepository.softDelete(id);
    return {
      statusCode: HttpStatus.OK,
      message: 'data berhasil dihapus!',
    };
  }
}
