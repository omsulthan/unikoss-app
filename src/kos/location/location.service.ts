import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { City } from '../entities/city.entity';
import { District } from '../entities/district.entity';
import { Province } from '../entities/province.entity';
import { Village } from '../entities/village.entity';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Province)
    private provinceRepository: Repository<Province>,
    @InjectRepository(City)
    private cityRepository: Repository<City>,
    @InjectRepository(District)
    private districtRepository: Repository<District>,
    @InjectRepository(Village)
    private villageRepository: Repository<Village>,
  ) {}

  async findAll() {
    const province = await this.provinceRepository.find();
    const city = await this.cityRepository.find({ relations: ['province'] });
    const district = await this.districtRepository.find({
      relations: ['city'],
    });
    const village = await this.villageRepository.find({ relations: ['Z'] });

    return {
      province,
      city,
      district,
      village,
    };
  }

  async findProvince() {
    const province = await this.provinceRepository.find({
      order: {
        name: 'ASC',
      },
    });
    return province;
  }
  async findAllCity() {
    const data = await this.cityRepository.find({
      order: {
        name: 'ASC',
      },
    });
    return data;
  }

  async findCity(id: string) {
    const city = await this.cityRepository.find({
      where: {
        province: {
          id,
        },
      },
      order: {
        name: 'ASC',
      },
    });
    return city;
  }

  async findDistrict(id: string) {
    const district = await this.districtRepository.find({
      where: {
        city: {
          id,
        },
      },
      order: {
        name: 'ASC',
      },
    });
    return district;
  }

  async findVillage(id: string) {
    const village = await this.villageRepository.find({
      where: {
        district: {
          id,
        },
      },
      order: {
        name: 'ASC',
      },
    });
    return village;
  }
}
