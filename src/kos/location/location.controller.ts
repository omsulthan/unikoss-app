import { Controller, Get, Param } from '@nestjs/common';
import { LocationService } from './location.service';

@Controller('location')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @Get('/province')
  getProvince() {
    return this.locationService.findProvince();
  }
  @Get('/city')
  getAllCity() {
    return this.locationService.findAllCity();
  }

  @Get('/city/:id')
  getCity(@Param('id') id: string) {
    return this.locationService.findCity(id);
  }

  @Get('/district/:id')
  getDistrict(@Param('id') id: string) {
    return this.locationService.findDistrict(id);
  }

  @Get('/village/:id')
  getVillage(@Param('id') id: string) {
    return this.locationService.findVillage(id);
  }
}
