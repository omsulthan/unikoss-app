import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { BoardingHouse } from '../entities/boarding_house.entity';
import { Review } from '../entities/review.entity';
import { CreateReviewDto } from './dto/create-review.dto';

@Injectable()
export class ReviewService {
  constructor(
    @InjectRepository(Review)
    private reviewRepository: Repository<Review>,
    @InjectRepository(BoardingHouse)
    private boardingRepository: Repository<BoardingHouse>,
    private usersService: UsersService,
  ) {}

  async create(createReviewDto: CreateReviewDto, id: string, req: string) {
    const userData = await this.usersService.findOne(req);
    const boarding = await this.boardingRepository
      .createQueryBuilder('b')
      .where('b.id = :id', { id })
      .getOne();

    const review = new Review();
    review.rating = createReviewDto.rating;
    review.comment = createReviewDto.comment;
    review.boarding = boarding;
    (review.user = userData), await this.reviewRepository.insert(review);

    const newBoarding = await this.boardingRepository.findOne({
      where: { id },
      relations: ['review'],
    });
    const allRate = newBoarding.review.map((v) => {
      return v.rating;
    });

    let length = allRate.length;
    let sum: number;
    function myFunc(total: number, num: number) {
      return total + num;
    }
    if (length == 0) {
      sum = 0;
    } else {
      sum = allRate.reduce(myFunc);
    }
    let avg = sum / length;

    this.boardingRepository.save({ ...newBoarding, rate: avg });

    return review;
  }

  async findOne(id: string) {
    const result = await this.reviewRepository.findOneOrFail({
      where: {
        id,
      },
      relations: ['user'],
    });

    return result;
  }

  async countReview() {
    const result = await this.reviewRepository
      .createQueryBuilder('review')
      .leftJoinAndSelect('review.boarding', 'boarding')
      .groupBy('boarding.id')
      .addGroupBy(' review.id')
      .orderBy('count(boarding.id)', 'DESC')
      .select('boarding.id, boarding.name')
      .execute();
    return result;
  }
}
