import { Module } from '@nestjs/common';
import { ReviewService } from './review.service';
import { ReviewController } from './review.controller';
import { BoardingHouse } from '../entities/boarding_house.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Review } from '../entities/review.entity';
import { UsersModule } from 'src/users/users.module';
import { User } from 'src/users/entities/user.entity';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([BoardingHouse, Review, User]),AuthModule, UsersModule],
  providers: [ReviewService,],
  controllers: [ReviewController]
})
export class ReviewModule {}
