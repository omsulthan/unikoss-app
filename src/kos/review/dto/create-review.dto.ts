import { IsNotEmpty, isNotEmpty, IsOptional } from "class-validator";

export class CreateReviewDto {
    @IsOptional()
    rating: number

    @IsOptional()
    comment: string

    // @IsNotEmpty()
    // boarding_id: string
}