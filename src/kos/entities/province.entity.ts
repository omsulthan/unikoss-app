import { Column, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { BoardingHouse } from "./boarding_house.entity";
import { City } from "./city.entity";

@Entity()
export class Province {
    @PrimaryColumn()
    id: string

    @Column()
    name: string

    @OneToMany(
        () => { return City },
        (city) => { return city.province}
    )
    city: City

    @OneToMany(
        () => { return BoardingHouse },
        (boarding) => { return boarding.province}
    )
    boarding: BoardingHouse
}