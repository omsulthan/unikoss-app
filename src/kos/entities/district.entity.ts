import { Column, Entity, ManyToOne, OneToMany, PrimaryColumn } from "typeorm";
import { BoardingHouse } from "./boarding_house.entity";
import { City } from "./city.entity";
import { Village } from "./village.entity";

@Entity()
export class District {
    @PrimaryColumn()
    id: string

    @Column()
    name: string

    @ManyToOne(
        () => { return City },
        (city) => { return city.district }
    )
    city: City

    @OneToMany(
        () => { return Village },
        (village) => { return village.district}
    )
    village: Village


    @OneToMany(
        () => { return BoardingHouse },
        (boarding) => { return boarding.district}
    )
    boarding: BoardingHouse
}