import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BoardingHouse } from "./boarding_house.entity";

@Entity()
export class Image {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column()
    name: string

    @ManyToOne(
        () => {
          return BoardingHouse;
        },
        (callback) => {
          return callback.image;
        },
      )
      boarding: BoardingHouse;
}