import { Transaction } from 'src/transaction/entities/transaction.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  IsNull,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { City } from './city.entity';
import { District } from './district.entity';
import { Facility } from './facility.entity';
import { Village } from './village.entity';
import { Province } from './province.entity';
import { Review } from './review.entity';
import { Image } from './image.entity';

@Entity()
export class BoardingHouse {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  type: string;

  @Column()
  address: string;

  @Column()
  status: string;

  @Column()
  price: number;

  @Column()
  description: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @Column()
  notes: string;

  @Column({
    type: 'decimal',
    precision: 2,
    scale: 1,
    default: 0,
  })
  rate;

  @ManyToOne(
    () => {
      return Province;
    },
    (province) => {
      return province.boarding;
    },
  )
  @JoinColumn()
  province: Province;

  @ManyToOne(
    () => {
      return City;
    },
    (city) => {
      return city.boarding;
    },
  )
  city: City;

  @ManyToOne(
    () => {
      return District;
    },
    (district) => {
      return district.boarding;
    },
  )
  district: District;

  @ManyToOne(
    () => {
      return Village;
    },
    (village) => {
      return village.boarding;
    },
  )
  village: Village;

  @ManyToOne(
    () => {
      return User;
    },
    (user) => {
      return user.id;
    },
  )
  user: User;

  @OneToMany(
    () => {
      return Transaction;
    },
    (transaction) => {
      return transaction.boarding;
    },
  )
  transaction: Transaction;

  @ManyToMany(() => Facility, (facility: Facility) => facility.boarding, {
    cascade: true,
    onUpdate: 'CASCADE',
  })
  @JoinTable({ name: 'list_boarding_facility' })
  facilities: Facility[];

  addFacility(facility: Facility) {
    if (this.facilities == null) {
      this.facilities = new Array<Facility>();
    }
    this.facilities.push(facility);
  }

  @OneToMany(
    () => {
      return Review;
    },
    (callback) => {
      return callback.boarding;
    },
  )
  review: Review[];

  @OneToMany(
    () => {
      return Image;
    },
    (callback) => {
      return callback.boarding;
    },
  )
  image: Image[];
}
