import { Column, Entity, ManyToOne, OneToMany, PrimaryColumn } from "typeorm";
import { BoardingHouse } from "./boarding_house.entity";
import { District } from "./district.entity";
import { Province } from "./province.entity";

@Entity()
export class City {
    @PrimaryColumn()
    id: string
    
    @Column() 
    name: string

    @ManyToOne(
        () => { return Province },
        (province) => { return province.city }
    )
    province: Province

    @OneToMany(
        () => { return District },
        (district) => { return district.city}
    )
    district: District

    @OneToMany(
        () => { return BoardingHouse },
        (boarding) => { return boarding.city }
    )
    boarding: BoardingHouse

}