import { Column, Entity, ManyToOne, OneToMany, PrimaryColumn } from "typeorm";
import { BoardingHouse } from "./boarding_house.entity";
import { District } from "./district.entity";

@Entity()
export class Village {
    @PrimaryColumn()
    id: string

    @Column() 
    name: string

    @ManyToOne(
        () => { return District },
        (callback) => { return callback.village }
    )
    district: District

    @OneToMany(
        () => { return BoardingHouse },
        (boarding) => { return boarding.village }
    )
    boarding: BoardingHouse

}