import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { BoardingHouse } from "./boarding_house.entity";

@Entity()
export class Facility {
    @PrimaryGeneratedColumn('uuid')
    id: string 
    
    @Column()
    name: string

    @Column()
    typeof_facility: string

    @ManyToMany(() => BoardingHouse, (boarding: BoardingHouse) => boarding.facilities)
    boarding: BoardingHouse;

    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;
  
    @DeleteDateColumn()
    deletedAt: Date;

}