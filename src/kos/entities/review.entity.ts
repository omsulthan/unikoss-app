import { Reply } from 'src/users/entities/reply.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BoardingHouse } from './boarding_house.entity';

@Entity()
export class Review {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  rating: number;

  @Column()
  comment: string;

  @ManyToOne(
    () => {
      return BoardingHouse;
    },
    (callback) => {
      return callback.id;
    },
  )
  boarding: BoardingHouse;

  @ManyToOne(
    () => {
      return User
    },
    (callback) => {
      return callback.id
    }
  )
  user : User

  @OneToMany(
    () => {
      return Reply;
    },
    (callback) => {
      callback.id;
    },
  )
  reply: Reply;
}
