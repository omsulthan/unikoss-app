import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BoardingHouse } from './entities/boarding_house.entity';
import { City } from './entities/city.entity';
import { District } from './entities/district.entity';
import { Facility } from './entities/facility.entity';
import { Image } from './entities/image.entity';
import { Village } from './entities/village.entity';
import { Province } from './entities/province.entity';
import { KosController } from './kos.controller';
import { KosService } from './kos.service';
import { FacilityController } from './facility/facility.controller';
import { FacilityService } from './facility/facility.service';
import { ReviewModule } from './review/review.module';
import { AuthModule } from 'src/auth/auth.module';
import { User } from 'src/users/entities/user.entity';
import { UsersModule } from 'src/users/users.module';
import { LocationController } from './location/location.controller';
import { LocationService } from './location/location.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Province,
      City,
      District,
      Village,
      Image,
      BoardingHouse,
      Facility,
      User,
    ]),
    AuthModule,
    UsersModule,
    ReviewModule,
  ],
  controllers: [KosController, FacilityController, LocationController],
  providers: [KosService, FacilityService, LocationService],
  exports: [KosService],
})
export class KosModule {}
