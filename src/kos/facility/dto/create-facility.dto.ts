import { IsNotEmpty } from "class-validator";

export class CreateFacilityDto {

    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    typeof_facility: string

}