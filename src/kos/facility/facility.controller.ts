import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { CreateFacilityDto } from './dto/create-facility.dto';
import { UpdateFacilityDto } from './dto/update-kos.dto';
import { FacilityService } from './facility.service';

@Controller('facility')
export class FacilityController {
  constructor(private readonly facilityService: FacilityService) {}

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.facilityService.remove(id);
  }

  @Get()
  findAll() {
    return this.facilityService.findAll();
  }

  @Get('/type/type')
  findByType() {
    return this.facilityService.findByType();
  }

  @Post()
  createFacility(@Body() createFacilityDto: CreateFacilityDto) {
    return this.facilityService.create(createFacilityDto);
  }

  @Put('/:id')
  updateFacility(
    @Param('id') id: string,
    @Body() updateFacilityDto: UpdateFacilityDto,
  ) {
    return this.facilityService.update(id, updateFacilityDto);
  }
  @Delete('/:id')
  deleteFacility(@Param('id') id: string) {
    return this.facilityService.remove(id);
  }
}
