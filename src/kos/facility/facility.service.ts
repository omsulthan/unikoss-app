import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { EntityNotFoundError, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Facility } from '../entities/facility.entity';
import { CreateFacilityDto } from './dto/create-facility.dto';
import { UpdateFacilityDto } from './dto/update-kos.dto';

@Injectable()
export class FacilityService {
    constructor(
        @InjectRepository(Facility)
        private facilityRepository: Repository<Facility>,
      ) {}
    
      async create(createFacilityDto: CreateFacilityDto) {
        const result = await this.facilityRepository.insert(createFacilityDto);
    
        return this.facilityRepository.findOneOrFail({
          where: {
            id: result.identifiers[0].id,
          },
        });
      }
    
      async findAll() {
        const facility = await this.facilityRepository.findAndCount();
        return {
          data: facility[0],
          count: facility[1]
        }
      }

      async findByType() {
        const privateFacility = await this.facilityRepository.findBy({typeof_facility: "fasilitas pribadi"});
        const publicFacility = await this.facilityRepository.findBy({typeof_facility: "fasilitas umum"});
        return {
          privateFacility,
          publicFacility,
        }
      }
    
      async findOne(id: string) {
        try {
          return await this.facilityRepository.findOneOrFail({
            where: {
              id,
            },
          });
        } catch (e) {
          if (e instanceof EntityNotFoundError) {
            throw new HttpException(
              {
                statusCode: HttpStatus.NOT_FOUND,
                error: 'Data not found',
              },
              HttpStatus.NOT_FOUND,
            );
          } else {
            throw e;
          }
        }
      }
    
      async update(id: string, updateFacilityDto: UpdateFacilityDto) {
        try {
          await this.facilityRepository.findOneOrFail({
            where: {
              id,
            },
          });
        } catch (e) {
          if (e instanceof EntityNotFoundError) {
            throw new HttpException(
              {
                statusCode: HttpStatus.NOT_FOUND,
                error: 'Data not found',
              },
              HttpStatus.NOT_FOUND,
            );
          } else {
            throw e;
          }
        }
    
        await this.facilityRepository.update(id, updateFacilityDto);
    
        return this.facilityRepository.findOneOrFail({
          where: {
            id,
          },
        });
      }
    
      async remove(id: string) {
        try {
          await this.facilityRepository.findOneOrFail({
            where: {
              id,
            },
          });
        } catch (e) {
          if (e instanceof EntityNotFoundError) {
            throw new HttpException(
              {
                statusCode: HttpStatus.NOT_FOUND,
                error: 'Data not found',
              },
              HttpStatus.NOT_FOUND,
            );
          } else {
            throw e;
          }
        }
    
        await this.facilityRepository.delete(id);
      }
}
