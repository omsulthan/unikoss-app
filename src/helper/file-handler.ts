import * as path from 'path';
import * as uuid from 'uuid';
import * as fs from 'fs';
import * as moment from 'moment';
import { HttpException, HttpStatus } from '@nestjs/common';

export const editFileName = (req, file, callback) => {
  // const name = file.originalname.split('.')[0];
  const fileExtName = path.extname(file.originalname);
  const name = uuid.v4();
  const dir = '../files';

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, {
      recursive: true,
    });
  }

  const d = new Date();

  if (
    [
      '.png',
      '.jpg',
      '.jpeg',
      '.JPEG',
      '.JPG',
      '.PNG',
      '.pdf',
      '.PDF',
      '.mp4',
      '.mp3',
    ].includes(fileExtName)
  ) {
    callback(
      null,
      `/${name}_${moment().format('YYYYMMDDHHmmss')}${fileExtName}`,
    );
  } else {
    callback(
      new HttpException(
        `Unsupported file type ${path.extname(
          file.originalname,
        )}, the document must be .pdf, .png, .jpg, or .jpeg`,
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
};
