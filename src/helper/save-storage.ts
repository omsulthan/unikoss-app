import { HttpStatus } from '@nestjs/common';
import { diskStorage } from 'multer';
import { extname } from 'path';

type validate =
  | 'image/png'
  | 'image/jpg'
  | 'image/jpeg'
  | 'image/webp'
  | 'image/svg';

const mimetype: validate[] = [
  'image/png',
  'image/jpg',
  'image/jpeg',
  'image/webp',
  'image/svg',
];

export const storage = {
  storage: diskStorage({
    destination: './files',
    filename: (req, file, callback) => {
      const randomName = Math.round(Math.random() * 1e9);
      const imageName = extname(file.originalname);
      const getImage = `${randomName}${imageName}`;
      callback(null, getImage);
    },
  }),
  __filename,
  fileFilter: (req, file, callback) => {
    const exist: validate[] = mimetype;

    exist.includes(file.mimetype)
      ? callback(null, true)
      : callback(null, false);
  },
};
