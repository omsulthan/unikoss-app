import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpStatus,
  UseGuards,
  Req,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('/profile')
  @UseGuards(AuthGuard())
  async getProfile(@Req() req) {
    const userData = await this.usersService.getProfile(req.user.id);

    return {
      name: userData.name,
      email: userData.email,
      phone: userData.phone_numbers,
      image: userData.image,
      role: userData.role.name,
      status: userData.status,
    };
  }

  // Get All Users
  @Get()
  async findAll() {
    const [data, count] = await this.usersService.findAll();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // Get User By Id
  @Get(':id')
  async findById(@Param('id') id: string) {
    return {
      data: await this.usersService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // List client and owner
  @Get('/role/:role')
  async findByRole(@Param('role') role: string) {
    return {
      data: await this.usersService.findByRole(role),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
  @Get('/status/:status')
  async findByStatus(@Param('status') role: string) {
    return {
      data: await this.usersService.findByStatus(role),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // Edit User
  @Put('/profile/edit')
  @UseGuards(AuthGuard())
  async updateUser(@Req() req, @Body() updateUserDto: UpdateUserDto) {
    await this.usersService.update(req.user.id, updateUserDto);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put(':role/:id')
  async updateRole(
    @Param('role') role: string,
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUSerDto: UpdateUserDto,
  ) {
    return {
      data: await this.usersService.updateRole(role, id, UpdateUserDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  // Update Status User owner
  @Put(':id')
  async updateStatus(@Param('id') id: string) {
    return {
      data: await this.usersService.updateStatus(id),
    };
  }
  @Put('/reject/owner/:id')
  async rejectStatus(@Param('id') id: string) {
    return {
      data: await this.usersService.rejectStatus(id),
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.usersService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
