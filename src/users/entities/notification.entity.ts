import {
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  Entity,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Notification extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(
    () => {
      return User;
    },
    (callback) => {
      return callback.id;
    },
  )
  sender: User;

  @ManyToOne(
    () => {
      return User;
    },
    (callback) => {
      return callback.id;
    },
  )
  reciever: User;

  @Column()
  message: string;

  @Column()
  status: string;

  @CreateDateColumn()
  craeted_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
