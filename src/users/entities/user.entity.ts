import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { Role } from './role.entity';
import { Transaction } from '../../transaction/entities/transaction.entity';
import { Reply } from './reply.entity';
import { Notification } from './notification.entity';
import { Review } from 'src/kos/entities/review.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  phone_numbers: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  status: string;

  @Column({ default: '' })
  image: string;

  @Column()
  salt: string;

  @ManyToOne(
    () => {
      return Role;
    },
    (callback) => {
      return callback.user;
    },
  )
  role: Role;

  @OneToMany(
    () => {
      return Transaction;
    },
    (tr) => {
      return tr.user;
    },
  )
  transaction: Transaction;

  @OneToMany(
    () => {
      return Reply;
    },
    (callback) => {
      return callback.reply;
    },
  )
  reply: Reply;

  @OneToMany(
    () => {
      return Notification;
    },
    (callback) => {
      return callback.sender;
    },
  )
  sender: Notification;

  @OneToMany(
    () => {
      return Notification;
    },
    (callback) => {
      return callback.reciever;
    },
  )
  reciever: Notification;

  @OneToMany(
    () => {
      return Review;
    },
    (callback) => {
      return callback.user;
    },
  )
  review: User;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
