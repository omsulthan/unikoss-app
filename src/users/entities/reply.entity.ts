import { Review } from 'src/kos/entities/review.entity';
import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Reply {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(
    () => {
      return User;
    },
    (callback) => {
      return callback.id;
    },
  )
  user: User;

  @ManyToOne(
    () => {
      return Review;
    },
    (callback) => {
      return callback.id;
    },
  )
  review: Review;

  @Column()
  reply: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;
}
