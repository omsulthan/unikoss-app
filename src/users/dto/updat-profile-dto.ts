import { IsOptional } from "class-validator";

export class UpdateProfileDto {
    @IsOptional()
    name: string

    @IsOptional()
    email: string

    @IsOptional()
    phone: string

    @IsOptional()
    image: string
}