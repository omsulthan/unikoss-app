import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { EntityNotFoundError, Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './entities/role.entity';
import { statusTransaction } from 'src/transaction/entities/transaction.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const result = await this.usersRepository.insert(createUserDto);

    return this.usersRepository.findOneOrFail({
      where: {
        id: result.identifiers[0].id,
      },
    });
  }
  // Get All User
  findAll() {
    const result = this.usersRepository
      .createQueryBuilder('user')
      .innerJoinAndSelect('user.role', 'role');

    return result.getManyAndCount();
  }

  // Get Id User Detail
  async findOne(id: string) {
    try {
      const dataUser = await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
        relations: ['role'],
      });

      return dataUser;
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  // Update User
  async update(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.usersRepository.update(id, updateUserDto);

    return this.usersRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  // Delete User
  async remove(id: string) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.usersRepository.softDelete(id);
  }

  // Get User By Role

  async updateRole(role: string, id: string, UpdateUserDto) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
      if (role == 'pemilik') {
        var result = '2dbc56ee-bd9f-4b58-b84e-963770c759f2';
      } else if (role == 'penyewa') {
        var result = '161a35e7-e77a-4c24-9aca-7e4e54fbdc97';
      } else if (role == 'blokir') {
        var result = '8cc3e45f-8580-47b1-9518-1f94a5381901';
      } else if (role != 'pemilik' && 'penyewa' && 'blokir') {
        throw new HttpException(
          {
            statusCode: HttpStatus.METHOD_NOT_ALLOWED,
            message: `Method ${role} Not Found`,
          },
          HttpStatus.NOT_FOUND,
        );
      }
      await this.usersRepository.save({
        id,
        role: {
          id: result,
        },
        UpdateUserDto,
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    return this.usersRepository.findOneOrFail({
      where: {
        id,
      },
    });
  }

  // Update Status owner
  async updateStatus(id: string) {
    try {
      return await this.usersRepository.save({
        id: id,
        status: 'aktif',
      });
    } catch (e) {
      console.log(e);
    }
  }
  async rejectStatus(id: string) {
    try {
      return await this.usersRepository.save({
        id: id,
        status: 'tertolak',
      });
    } catch (e) {
      console.log(e);
    }
  }

  async findByRole(name: string) {
    try {
      return await this.usersRepository.findAndCount({
        where: {
          status: statusTransaction.TERTUNDA,
          role: {
            name,
          },
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findByStatus(status: string) {
    try {
      return await this.usersRepository.find({
        where: {
          status,
        },
        relations: ['role'],
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async createNotification(id: string) {}

  getProfile(id: string) {
    try {
      return this.usersRepository.findOne({
        where: { id },
        relations: ['role'],
      });
    } catch (error) {
      throw error;
    }
  }
}
