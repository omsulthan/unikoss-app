import { Controller, Get, Param, Res } from '@nestjs/common';

@Controller('upload')
export class UploadController {
  @Get('/:imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './files' });
  }
}
