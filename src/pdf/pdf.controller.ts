import { Body, Controller, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';
import { PdfService } from './pdf.service';

@Controller('pdf')
export class PdfController {
  constructor(private pdfService: PdfService) {}

  @Get('/:id')
  async generatePdf(@Res() res: Response, @Param('id') id: string) {
    const generate = await this.pdfService.getPdf(id);
    return res.download(generate, 'report.pdf');
  }
}