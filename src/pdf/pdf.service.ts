import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { randomStringGenerator } from '@nestjs/common/utils/random-string-generator.util';
import { format } from 'date-fns';
import * as pdf from 'pdf-creator-node';
import * as fs from 'fs';
import { TransactionService } from 'src/transaction/transaction.service';
import formatDate from 'src/helper/formatDate'
import formatCurrency from 'src/helper/formatCurrency'

@Injectable()
export class PdfService {
    constructor(
    private trasactionService: TransactionService 
  ) {}
  async getPdf(id: string) {
    const transaction = await this.trasactionService.findById(id)
    const date = formatDate(transaction.created_at)
    const name = transaction.boarding.name
    const address = transaction.boarding.address
    const price = formatCurrency(transaction.boarding.price)
    const total = formatCurrency(transaction.price)
    let tax = formatCurrency(transaction.boarding.price * (10/100))
    let dp = formatCurrency(transaction.price * (10/100))
    const startDate = formatDate(transaction.start_date)
    const endDate = formatDate(transaction.end_date)

    async function generatePdf(id, date, name, address, price, total, tax, dp, startDate, endDate) {
      // return this.readFile;

      const promise: Promise<any> = new Promise((resolve, reject) => {
        fs.readFile(
          __dirname + '/index.html',
          'utf-8',
          (err, data) => {
            if (err) {
              reject(
                new HttpException(
                  new Error(err.message),
                  HttpStatus.INTERNAL_SERVER_ERROR,
                ),
              );
            }

            const options = {
              format: 'a3',
              orientation: 'potrait',
              childProcessOptions: {
                env: {
                  OPENSSL_CONF: '/dev/null',
                },
              },
            };

            const random = `${randomStringGenerator()} - ${Date.now().toString()}.pdf`;

            const documents = {
              html: data,
              data: {
                id,
                date,
                name, 
                address,
                price, 
                total, 
                tax, 
                dp, 
                startDate, 
                endDate
              },
              path: 'docs/' + random,
              type: '',
            };

            pdf
              .create(documents, options)
              .then((res) => {
                console.log(res);

                resolve('docs/' + random);
              })
              .catch((err) => {
                reject(err);
              });
          },
        );
      });

      return promise;
    }

    const generated = await generatePdf(parseInt(id), date, name, address, price, total, tax, dp, startDate, endDate);

    return generated;
  }
}
