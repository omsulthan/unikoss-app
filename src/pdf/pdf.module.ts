import { Module } from '@nestjs/common';
import { PdfService } from './pdf.service';
import { PdfController } from './pdf.controller';
import { TransactionModule } from 'src/transaction/transaction.module';

@Module({
  providers: [PdfService],
  controllers: [PdfController],
  imports: [TransactionModule]
})
export class PdfModule {}
